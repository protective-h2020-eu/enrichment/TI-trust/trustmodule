from setuptools import setup

setup(name='protective_trustmodule',
      version='0.2',
      description='The trust computation module for PROTECTIVE.',
      author='Samuel Ruppert',
      author_email='samuel.ruppert@stud.tu-darmstadt.de',
      packages=['protective_trustmodule'],
      install_requires=[
          'jmespath',
          'numpy',
          'requests',
          'python-dateutil',
          'mmh3',
          'idea-format'
      ],
      zip_safe=False)
