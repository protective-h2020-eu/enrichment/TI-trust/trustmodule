import logging
import shelve
import sys
import signal
import numpy as np
from collections import deque

class MovingAveragePersistent:

    def __init__(self, filename, default_length, save_on_exit=True):
        self.save_on_exit = save_on_exit
        self.default_length = default_length
        self.sequences = {}

        try:
            self.storage = shelve.open(filename)
            self.sequences = self.storage['sequences']
        except:
            logging.warning("Could not load MovingAverage data from file. Starting from scratch...")
        signal.signal(signal.SIGINT, self.save_exit)
        signal.signal(signal.SIGTERM, self.save_exit)

    def length(self, key):
        if key not in self.sequences:
            return None

        return len(self.sequences[key])

    def max_length(self, key):
        if key not in self.sequences:
            return None

        return len(self.sequences[key].maxlen)

    def value(self, key):
        if key not in self.sequences:
            return None

        return np.sum(self.sequences[key]) / len(self.sequences[key])

    def add(self, key, number):
        if key not in self.sequences:
            self.sequences[key] = deque([], self.default_length)
        self.sequences[key].append(number)
        return self.value(key)

    def store(self, close=False):
        try:
            self.storage['sequences'] = self.sequences
            if close:
                self.storage.close()
            else:
                self.storage.sync()
            logging.info("Saved MovingAverage to file.")
        except:
            logging.error("Could not save MovingAverage data to file.")

    def save_exit(self, signum, stack):
        self.store(close=True)
        sys.exit(signum)

    def __del__(self):
        self.store(close=True)