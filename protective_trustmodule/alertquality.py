import itertools
import numpy as np
import jmespath
from datetime import datetime
from dateutil import parser as datetime_parser
from idea.lite import Idea
from .cmsketch import CMSketchPersistentRotation
from .movingaverage import MovingAveragePersistent
from .util import AbstractConfig, parse_alert


class AlertQuality:
    """This class calculates the alert quality
    """

    def __init__(self, alert_config=None):

        # store configuration
        self.cfg = AlertQualityConfig() if isinstance(alert_config, AlertQualityConfig) else alert_config

        # setup count-min sketch and load data from file if it exists
        self.sketch = CMSketchPersistentRotation(
            self.cfg.IPR_SKETCH_LENGTH,
            self.cfg.IPR_SKETCH_FILE,
            self.cfg.IPR_SKETCH_WIDTH,
            self.cfg.IPR_SKETCH_DEPTH,
            True)

        # setup persistent moving average object and load data from file if it exists
        self.priors = MovingAveragePersistent(self.cfg.ST_MOVING_AVG_FILE, self.cfg.ST_MOVING_AVG_LENGTH_MAX)

    def get_quality(self, alert):
        """Alert quality score computation.

        Arguments:
            alert {dict|string} -- Dictionary or JSON string representing a dictionary of the alert (IDEA format).

        Returns:
            object -- Returns the resulting outputs as object.
        """

        # parse alert if it's a JSON string
        alert = parse_alert(alert)

        ###### CPL (COMPLETENESS) ######
        cpl = self.completeness(alert)

        ###### AF (ALERT FRESHNESS) ######
        af = self.alert_freshness(alert)

        ###### SR (SOURCE RELEVANCE) ######
        sr_value, sr_value_weighted = self.source_relevance(alert)

        ###### ST (SOURCE TRUSTWORTHINESS) ######
        st_source, st_prior = self.source_trustworthiness(alert)

        ###### OPINION (CPL discounted by AF, Certainty, SourceTrustworthiness) ######
        opinion = {}
        opinion["Quality"] = float((1 - self.cfg.AF_WEIGHT) * cpl + self.cfg.AF_WEIGHT * af)
        opinion["Certainty"] = float(sr_value_weighted)
        opinion["SourceTrustworthiness"] = float(st_prior)

        ###### SCORE (final quality score based on all inputs) ######
        score = opinion["Quality"] * opinion["Certainty"] + opinion["SourceTrustworthiness"] * (1 - opinion["Certainty"])

        ###### if we have a source name, add the score to its moving average ######
        if st_source:
            self.priors.add(st_source, score)

        return {
            "Score": float(score),
            "Inputs": {
                "Completeness": float(cpl),
                "SourceRelevance": float(sr_value),
                "AlertFreshness": float(af)
            },
            "Opinion": opinion,
            "IpRecurrence": int(self.ip_recurrence(alert))
        }

    def completeness(self, alert):
        """Determine alert completeness over all possible fields in IDEA format.

        Arguments:
            alert {dict|string} -- Dictionary or JSON string representing a dictionary of the alert (IDEA format).

        Returns:
            float -- Completeness of the IDEA alert.
        """

        # load completeness IDEA schema
        typedef_fields = set(Idea.typedef)

        # make sure we have a typedef for the IDEA schema.
        if len(typedef_fields) < 0:
            logging.error("Could not load idea fields from Idea.typedef.")
            raise Exception("Could not load idea fields from Idea.typedef.")

        # find non-missing fields in alert
        non_missing_fields = typedef_fields & set(alert.keys())

        # un-weighted completeness over all possible fields in percent
        cpl_percent = len(non_missing_fields) / len(typedef_fields)

        # check important fields
        cpl_boost = 0.0
        cpl_boost_total = 0.0
        for field, weight in self.cfg.CPL_IMPORTANT_FIELDS.items():
            cpl_boost_total += weight
            if jmespath.search(field, alert):
                cpl_boost += weight

        # calculate boost between [0;CPL_IMPORTANT_MAX_BOOST] with 0 <= CPL_IMPORTANT_MAX_BOOST <= 1
        cpl_boost = cpl_boost / cpl_boost_total * self.cfg.CPL_IMPORTANT_MAX_BOOST

        # boosted completeness
        return cpl_percent * (1 + cpl_boost)

    def source_relevance(self, alert):
        """Determines source type from alert message and returns source relevance.

        Arguments:
            alert {dict|string} -- Dictionary or JSON string representing a dictionary of the alert (IDEA format).

        Returns:
            tuple -- Source relevance (value, value_weighted_by_certainty)
        """

        # get nodes (i.e. alert source / sensor)
        nodes = jmespath.search(self.cfg.IDEA_NODE, alert)

        # return if no Node field can be found in the alert (certainty is 1)
        if not nodes:
            return (self.cfg.SR_VALUE_MISSING, self.cfg.SR_VALUE_MISSING)

        # convert node details to lower case for lookup
        nodes = [str(n).lower() for n in nodes]

        matches = {}

        # iterate all node types specified in AlertQualityConfig
        for type, obj in self.cfg.SR_NODE_TYPES.items():
            # for the current node type count the total number of keyword occurrences in node field
            matches[type] = sum([1 for pair in list(itertools.product([k.lower() for k in obj['keywords']], nodes)) if pair[0] in pair[1]])

        # return if we have no matching keywords (certainty is 1)
        if sum(matches.values()) <= 0:
            return (self.cfg.SR_VALUE_UNKNOWN, self.cfg.SR_VALUE_UNKNOWN)

        node_type = max(matches.keys(), key=lambda k: matches[k])

        # return if we cannot determine node type (certainty is 1)
        if not node_type:
            return (self.cfg.SR_VALUE_UNKNOWN, self.cfg.SR_VALUE_UNKNOWN)

        # get min and max weight for normalization
        max_weight = self.cfg.SR_NODE_TYPES[max(self.cfg.SR_NODE_TYPES, key=lambda k: self.cfg.SR_NODE_TYPES[k]['weight'])]['weight']
        min_weight = self.cfg.SR_NODE_TYPES[min(self.cfg.SR_NODE_TYPES, key=lambda k: self.cfg.SR_NODE_TYPES[k]['weight'])]['weight']

        # make sure we have a positive max weight in config
        if max_weight <= 0:
            logging.error("No positive weights specified in SR_NODE_TYPES")
            raise ValueError("No positive weights specified in SR_NODE_TYPES")

        # assume node type with most keyword occurrences and calculate value, certainty and weighted value
        value = self.cfg.SR_NODE_TYPES[node_type]['weight'] / max_weight
        certainty = matches[node_type] / sum(matches.values())
        value_weighted_by_certainty = value * certainty + (1 - min_weight / 2) * (1 - certainty)

        return (value, value_weighted_by_certainty)

    def source_trustworthiness(self, alert):
        """Calculates moving average per source used as initial trust value (prior)

        Arguments:
            alert {dict|string} -- Dictionary or JSON string representing a dictionary of the alert (IDEA format).

        Returns:
            tuple -- Tuple containing source name and initial trust value (source, prior)
        """

        node_name = jmespath.search(self.cfg.IDEA_NODE_NAME, alert)

        # if node field or hostname is missing return default value
        if not node_name:
            return (None, self.cfg.ST_DEFAULT)

        # get domain name from node hostname
        source = node_name[:node_name.find('.', node_name.find('.') + 1)]

        prior = self.cfg.ST_DEFAULT

        prior_length = self.priors.length(source)
        if prior_length is not None and prior_length >= self.cfg.ST_MOVING_AVG_LENGTH_MIN:
            prior = self.priors.value(source)
            if prior is None:
                prior = self.cfg.ST_DEFAULT

        return (source, prior)

    def alert_freshness(self, alert):
        """Get the alert freshness from timestamps.

        Arguments:
            alert {dict|string} -- Dictionary or JSON string representing a dictionary of the alert (IDEA format).

        Returns:
            float -- Alert freshness
        """

        # get timedelta since alert detection
        _time = self.get_time(alert)

        # return zero if no time was found in alert
        if not _time:
            return 0.0

        # time delta in hours
        delta = datetime.utcnow() - _time
        hours = np.float16(delta.total_seconds() / 3600)

        # return zero if time is in the future
        if hours < 0:
            return 0.0

        # function for steps (i.e. time intervals)
        f_steps = lambda x: round(self.cfg.AF_INTERVAL_START_HOURS * x**1.25)

        # function for freshness values per step
        f_values = lambda x: 1 - 0.5 * (x / (self.cfg.AF_INTERVAL_COUNT - 1))**2

        # function for freshness after steps
        f_default = lambda x: 0.5**(x * self.cfg.AF_DECAY_FACTOR)+1

        # calculate freshness, assign discrete value if within one of the intervals (first n hours), otherwise use default function.
        af = np.piecewise(
            hours,
            [f_steps(i - 1) < hours < f_steps(i) for i in range(1, self.cfg.AF_INTERVAL_COUNT + 1)],
            list(map(f_values, range(self.cfg.AF_INTERVAL_COUNT))) + [f_default]
        )

        # return as float
        return float(af)

    def ip_recurrence(self, alert):
        """Get the IP recurrence for the given alert's ip address

        Arguments:
            alert {dict|string} -- Dictionary or JSON string representing a dictionary of the alert (IDEA format).

        Returns:
            int -- IP recurrence
        """

        # get source IPs from alert
        src_ips = jmespath.search(self.cfg.IDEA_SOURCE_IP, alert)

        # return 0 positive matches if no source ip is present in alert.
        if not src_ips:
            return 0

        # we currently only take the first IP address into account
        ip = str(src_ips[0][0])

        # add ip to count-min sketch
        self.sketch.add(ip)

        # return ip recurrence from count-min sketch
        return self.sketch.query(ip)

    def get_time(self, alert):
        """Get the UTC time from a given alert.

        Arguments:
            alert {dict|string} -- Dictionary or JSON string representing a dictionary of the alert (IDEA format).

        Returns:
            datetime -- Datetime object for the alert or None if no time could be found.
        """

        try:
            # get time from alert
            _time = jmespath.search(self.cfg.IDEA_TIME, alert)

            # if not a datetime object parse it first
            if not isinstance(_time, datetime):
                _time = datetime_parser.parse(_time)

            # cut timezone suffix and return
            return _time.replace(tzinfo=None)

        except:
            return None


class AlertQualityConfig(AbstractConfig):
    """This class holds the configuration for alert quality calculations
    """

    # fields in IDEA alert
    IDEA_TIME = "DetectTime||EventTime||CreateTime||WinStartTime"
    IDEA_SOURCE_IP = "Source[*].[IP4,IP6][]"
    IDEA_NODE = "Node[*].*"
    IDEA_NODE_NAME = "Node[0].Name"
    IDEA_ALERT_QUALITY = "AlertQuality"

    # count-min sketch settings for IP recurrence
    IPR_SKETCH_LENGTH = 10000
    IPR_SKETCH_WIDTH = 10000
    IPR_SKETCH_DEPTH = 10
    IPR_SKETCH_FILE = "ipr_cmsketch.bin"

    # paths and weights for completeness of important fields in IDEA alert.
    CPL_IMPORTANT_WEIGHT = 2.0
    CPL_IMPORTANT_MAX_BOOST = 0.5
    CPL_IMPORTANT_FIELDS = {
        "Target": CPL_IMPORTANT_WEIGHT,
        "Target[*].Port": CPL_IMPORTANT_WEIGHT,
        "Target[*].Proto || Source[*].Proto": CPL_IMPORTANT_WEIGHT,
        "Category": CPL_IMPORTANT_WEIGHT,
        "Node": CPL_IMPORTANT_WEIGHT,
        "DetectTime||EventTime||CreateTime||WinStartTime": CPL_IMPORTANT_WEIGHT
    }

    # number of time intervals used to have discrete alert freshness if only a few hours old (must be 1 or greater)
    AF_INTERVAL_COUNT = 6
    # start size of time intervals in hours (i.e. first interval is 6 hours)
    AF_INTERVAL_START_HOURS = 6
    # weight of alert freshness for quality computation (CPL vs AF)
    AF_WEIGHT = 0.4
    # exponential decay factor
    AF_DECAY_FACTOR = 1/1600

    # number of alerts to calculate the moving average from
    ST_DEFAULT = 0.5
    ST_MOVING_AVG_LENGTH_MAX = 10000
    ST_MOVING_AVG_LENGTH_MIN = 100
    ST_MOVING_AVG_FILE = "st_movavg.bin"

    # default value if node type cannot be determined
    SR_VALUE_UNKNOWN = 0.25
    # value if node field is missing in alert (must be greater zero)
    SR_VALUE_MISSING = 0.1
    # weights and keywords for different source types used to calculate source relevance value
    SR_NODE_TYPES = {
        "honeypot": {"weight": 4, "keywords": [
            "tarpit",
            "honeypot",
            "kippo",
            "dionaea",
            "labrea",
            "nemea",
            "hostage",
            "cowrie",
            "conpot",
            "kojoney",
            "honeyd",
            "heralding",
            "amun",
            "glastopf"
        ]},
        "ids": {"weight": 3, "keywords": [
            "sentinel",
            "snort",
            "suricata",
            "bro",
            "zeek"
        ]},
        "firewall": {"weight": 2, "keywords": [
            "blacklist",
            "fail2ban",
            "uceprot",
            "brute_force_detector"
        ]}
    }
