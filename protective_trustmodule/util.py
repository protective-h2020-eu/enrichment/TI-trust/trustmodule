import json
import logging


def parse_alert(alert):
    """Check alert and parse if it's a JSON string.

    Arguments:
        alert {string|dict} -- JSON string or dict representing a dictionary of the alert (IDEA format).
    """

    # de-serialize alert if JSON string
    if isinstance(alert, dict):
        return alert

    try:
        return json.loads(alert)
    except Exception as e:
        logging.error("Invalid alert could not be parsed: {}".format(str(e)))
        raise

def flatten_list(list):
    """Flattens a nested list.

    Arguments:
        list {list} -- Any python list.
    """

    for elem in list:
        if isinstance(elem, type(list)):
            yield from flatten_list(elem)
        else:
            yield elem

class AbstractConfig:
    """This is the abstract class definition for config objects.
    """

    # initializes instance and sets attributes for every given argument overwriting defaults
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if(key in type(self).__dict__):
                self.__setattr__(key, value)

    # returns JSON serialization of config instance
    def __repr__(self):
        keys = [k for k in dir(self) if k[0] != "_"]
        values = [self.__getattribute__(k) for k in keys]
        return json.dumps(dict(zip(keys, values)),
                          sort_keys=True,
                          indent=4)
