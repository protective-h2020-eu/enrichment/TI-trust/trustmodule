import mmh3
import math
import numpy as np
import shelve
import signal
import sys
import logging
from collections import deque


class CMSketch(object):

    def __init__(self):
        self.width = None
        self.depth = None
        self.delta = None
        self.epsilon = None
        self.n = 0
        self.table = None

    def setWidthAndDepth(self, width, depth):
        self.width = width
        self.depth = depth
        # Calculate the other parameters
        self.delta = 1 / (math.exp(self.depth))
        self.epsilon = 2 / self.width
        # Set the table
        self.table = np.zeros(shape=(self.depth, self.width), dtype=np.int32)

    def setDeltaAndEpsilon(self, delta, epsilon):
        self.delta = delta
        self.epsilon = epsilon
        # Calculate the width and depth
        self.width = math.ceil(2.0 / self.epsilon)
        self.depth = math.ceil(math.log(1.0 / self.delta))
        # Set the table
        self.table = np.zeros(shape=(self.depth, self.width), dtype=np.int32)

    def add(self, x, value=1):
        if value == 0:
            return
        self.n += value
        for i in range(self.depth):
            bucket = self._hash(x, i)
            self.table[i, bucket] += value

    def query(self, x):
        counts = (self.table[i, self._hash(x, i)] for i in range(self.depth))
        return min(counts)

    def _hash(self, x, index):
        h64u, h64l = mmh3.hash64(x)
        return (h64l + index * h64u) % self.width

    def __getitem__(self, x):
        return self.query(x)

    def __len__(self):
        return self.n


class CMSketchRotation(CMSketch):

    def __init__(self, rotation_length):
        super(CMSketchRotation, self).__init__()
        self.journal_length = rotation_length
        self.journal = deque([], self.journal_length)

    def add(self, x):

        journal_full = (len(self.journal) >= self.journal_length)
        newBuckets = []

        for i in range(self.depth):
            # calculate bucket index
            bucket = self._hash(x, i)
            # append bucket index
            newBuckets.append(bucket)
            # increase bucket at index
            self.table[i, bucket] += 1
            # if journal max length is reached, remove oldest entry
            if journal_full:
                self.table[i, self.journal[0][i]] -= 1

        if not journal_full:
            self.n += 1

        # add entry to journal and remove oldest if max length is reached
        self.journal.append(newBuckets)


class CMSketchPersistentRotation(CMSketchRotation):

    def __init__(self, rotation_length, filename, width, depth, save_on_exit=True):
        super(CMSketchPersistentRotation, self).__init__(rotation_length)
        self.save_on_exit = save_on_exit
        try:
            self.storage = shelve.open(filename)
            self.width = self.storage['width']
            self.depth = self.storage['depth']
            self.delta = self.storage['delta']
            self.epsilon = self.storage['epsilon']
            self.n = self.storage['n']
            self.table = self.storage['table']
            self.journal_length = self.storage['journal_length']
            self.journal = self.storage['journal']
        except:
            logging.warning("Could not load CMSketch data from file. Using a new CMSketch...")
            self.setWidthAndDepth(width, depth)
        signal.signal(signal.SIGINT, self.save_exit)
        signal.signal(signal.SIGTERM, self.save_exit)

    def store(self, close=False):
        try:
            self.storage['width'] = self.width
            self.storage['depth'] = self.depth
            self.storage['delta'] = self.delta
            self.storage['epsilon'] = self.epsilon
            self.storage['n'] = self.n
            self.storage['table'] = self.table
            self.storage['journal'] = self.journal
            self.storage['journal_length'] = self.journal_length
            if close:
                self.storage.close()
            else:
                self.storage.sync()
            logging.info("Saved CMSketch to file.")
        except:
            logging.error("Could not save CMSketch data to file.")

    def save_exit(self, signum, stack):
        self.store(close=True)
        sys.exit(signum)

    def __del__(self):
        self.store(close=True)
