import requests
import jmespath
from .util import *


class NerdClient:
    """This class retrieves the entity reputation (FMP score) from NERD API
    """

    def __init__(self, nerd_config=None):

        # store configuration
        self.cfg = NerdClientConfig() if nerd_config is None else nerd_config
        
        # check if API key is set
        if not self.cfg.API_TOKEN:
            logging.warning("NERD API key not configured, IP reputation score will not be fetched.")

    def score(self, alert):
        """Retrieves the FMP score from the NERD API for the alert's source ip address.
        
        Arguments:
            alert {dict|string} -- Dictionary or JSON string representing a dictionary of the alert (IDEA format).

        Returns:
            float -- The entity FMP score from NERD API or None if alert has no source ip.
        """

        # if API key is not set, do nothing
        if not self.cfg.API_TOKEN:
            return None

        # de-serialize alert if JSON string
        alert = parse_alert(alert)

        # get source IPs from alert
        src_ips = jmespath.search(self.cfg.IDEA_SOURCE_IP, alert)

        if src_ips is None:
            logging.warning("No source IPs found in alert.")
            return None

        # flatten source IPs and remove duplicates
        src_ips = list(set(flatten_list(src_ips)))

        # in case we want to retrieve scores for multiple source IPs
        #
        # loop source IPs and retrieve a reputation score for each one from NERD API
        # scores = {}
        # for ip in src_ips:
        #     response = requests.get(self.cfg.API_URL + "/ip/" + ip,
        #                             headers={'Authorization': 'token ' + self.cfg.API_TOKEN})

        #     if response.status_code == 200:
        #         scores[ip] = response.json()['rep']
        #     else:
        #         logging.warning("No entity reputation score found for ip '{}': Status code {}".format(ip, response.status_code))
        #         scores[ip] = None
        # rep_scores = list(filter(None, scores.values()))

        response = requests.get(self.cfg.API_URL.replace("<ip>", src_ips[0]),
                                headers={'Authorization': 'token ' + self.cfg.API_TOKEN})

        if response.status_code != 200:
            logging.warning("No entity reputation score found for ip '{}': Status code {}".format(src_ips[0], response.status_code))
            return None
        try:
            return response.json()['fmp']['general']
        except KeyError:
            logging.warning("No entity reputation score found for ip '{}'".format(src_ips[0]))
            return None


class NerdClientConfig(AbstractConfig):
    """This class holdes the configuration for the NERD client
    """

    # use <ip> as placeholder for the ip address
    API_URL = "https://nerd.cesnet.cz/nerd/api/v1/ip/<ip>/fmp"
    API_TOKEN = ""

    IDEA_SOURCE_IP = "Source[*].IP4"
    IDEA_ENTITY_REPUTATION = "EntityReputation"
