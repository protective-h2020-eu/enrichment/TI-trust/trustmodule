# Trust Module

This is the trust module which provides functionality for calculating the alert quality as well as the entity reputation of an alert. It is intended to be used as part of the TrustEnricher plugin for the Mentat Enricher component.

# Prerequisites

This python module can be run stand-alone. See `requirements.txt` for a list of 3rd party dependencies. Tested with Python 3.6.4.

# Installation

The python module can be installed locally using pip:

1. `git clone https://gitlab.com/protective-h2020-eu/enrichment/TI-trust/trustmodule.git`
2. `cd trustmodule`
3. `pip3 install .`

# Usage

The `AlertQuality` class can be used to parse an alert and calculate its quality score as follows:

```python
# Create a AlertQualityConfig object and override default configuration as needed
config = AlertQualityConfig(IPR_SKETCH_FILE='ipr_data.bin', AF_WEIGHT=0.3)

# Create AlertQuality object
alert_quality = AlertQuality(config)

# Get quality from json-formatted raw alert
alert_json = '{...}'
quality = alert_quality.get_quality(alert_json)

```

The `NerdClient` class can be used to parse an alert and retrieve its entity reputation score from NERD API as follows:

```python
# Create a NerdClientConfig object and override default configuration as needed
config = NerdClientConfig(API_TOKEN='<API TOKEN>')

# Create NerdClient object
nerd_client = NerdClient(config)

# Get entity reputation score from NERD API for json-formatted alert
alert_json = '{...}'
entity_reputation = nerd_client.score(alert_json)

```

# Configuration

All configuration settings and their current default values are listed and explained below:

| Setting | Default | Description |
|----------------|---------|---------|
| IDEA_TIME | <code>"DetectTime&#124;&#124;EventTime&#124;&#124;CreateTime&#124;&#124;WinStartTime"</code> | Search pattern (jmespath) for the alert's detection time. If DetectTime is missing EventTime, CreateTime or WinStartTime is used instead. |
| IDEA_SOURCE_IP | <code>"Source[\*].[IP4,IP6][]"</code> | Search pattern (jmespath) for the source ip addresses |
| IDEA_NODE | <code>"Node[\*].\*"</code> | Search pattern (jmespath) for the alert's node field. |
| IDEA_NODE_NAME | <code>"Node[0].Name"</code> | Search pattern (jmespath) for the Name subfield in the alert's node field. |
| IDEA_ALERT_QUALITY | <code>"AlertQuality"</code> | Only used to name the result object when adding to the IDEA-formatted alert.  |
| IPR_SKETCH_LENGTH | <code>10000</code> | Length of the count-min sketch for ip recurrence. |
| IPR_SKETCH_WIDTH | <code>10000</code> | Width of the count-min sketch for the ip recurrence. |
| IPR_SKETCH_DEPTH | <code>10</code> | Depth of the count-min sketch for ip recurrence. |
| IPR_SKETCH_FILE | <code>"ipr_cmsketch.bin"</code> | Path to the file in which the count-min sketch for ip recurrence is stored. |
| CPL_IMPORTANT_WEIGHT | <code>2</code> | Standard weight for the important IDEA fields.  |
| CPL_IMPORTANT_MAX_BOOST | <code>0.5</code> | Maximum boost for completeness if all important fields are present. (must be between 0-1) |
| CPL_IMPORTANT_FIELDS | <code>{ 'Target[\*].Port': CPL_IMPORTANT_WEIGHT, ... }</code> | JSON object with search patterns (jmespath) for all important fields and their respective weights in case distinct values are prefered. |
| AF_INTERVAL_COUNT | <code>6</code> | Number of time intervals used to have discrete alert freshness if the alerts is only a few hours old (must be 1 or greater) |
| AF_INTERVAL_START_HOURS | <code>6</code> | Start size of the time intervals in hours (i.e. first interval is 6 hours) The following intervals dynamically increase in size. |
| AF_WEIGHT | <code>0.4</code> | Weight of the alert freshness when combining with completeness. |
| AF_DECAY_FACTOR | <code>1/1600</code> | Factor for exponential decay after discrete time intervals. |
| ST_DEFAULT | <code>0.5</code> | Default value for source trustwothiness when the alert's source is unclear or there are not enough alerts from this source to determine it's trustworthiness yet. (must be between 0-1) |
| ST_MOVING_AVG_LENGTH_MAX | <code>10000</code> | Maximum number of alerts from a source used to determine its source trustworthiness. |
| ST_MOVING_AVG_LENGTH_MIN | <code>100</code> | Minimum number of alerts from a source needed to determine its source trustworthiness. Before this number is reached ST_DEFAULT is used. |
| ST_MOVING_AVG_FILE | <code>"st_movavg.bin"</code> | Path to the file in which the moving averages for source trustworthiness are stored. |
| SR_VALUE_UNKNOWN | <code>0.25</code> | Default value for source relevance if node type (i.e. sensor) cannot be determined. |
| SR_VALUE_MISSING | <code>0.1</code> | Source relevance value if node field is missing in alert. (must be greater 0) |
| SR_NODE_TYPES | see below | JSON object containing different node/sensor types, their weight and the keywords used to identify them. Source relevance is then determined according to the weight. |
```python
{
   "honeypot": {"weight": 4, "keywords": [
      "tarpit",
      "honeypot",
      "kippo",
      "dionaea",
      "labrea",
      "nemea",
      "hostage",
      "cowrie",
      "conpot",
      "kojoney",
      "honeyd",
      "heralding",
      "amun",
      "glastopf"
   ]},
   "ids": {"weight": 3, "keywords": [
      "sentinel",
      "snort",
      "suricata",
      "bro",
      "zeek"
   ]},
   "firewall": {"weight": 2, "keywords": [
      "blacklist",
      "fail2ban",
      "uceprot",
      "brute_force_detector"
   ]}
}
```

Note: We recommend to update the list `SR_NODE_TYPES` with regards to the deployed sensor types and/or their hostnames in order to ensure accurate identification of the source type.  

# Dependencies

| Python Package | Version | License |
|----------------|---------|---------|
| jmespath       | 0.9.3	| MIT License (MIT) |
| numpy          | 1.14.0  | OSI Approved (BSD) |
| requests       | 2.18.4  | Apache Software License (Apache 2.0) |
| python-dateutil| 2.6.1   | Apache Software License, BSD License (Dual License) |
| mmh3           | 2.5.1   | CC0 1.0 Universal (CC0 1.0) Public Domain Dedication (License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication) |
| idea-format    | 0.1.13  | ISC License (ISCL) (ISC) |

